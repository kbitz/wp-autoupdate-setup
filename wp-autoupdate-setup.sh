#!/bin/bash

# Set script to only run as root
if [[ $EUID -ne 0 ]]; then
   echo -e "\e[1;31mThis script must be run as root!\e[0m" 1>&2
   exit 1
fi

echo "This script will install the necessary components to allow Wordpress to autoupdate via SSH."
echo "Note: this script will modify your php.ini configuration file."
echo "Start Installation? [y]"
read start
if [ "$start" == "n" ] || [ "$start" == "no" ]
	then
        	echo "Exiting..."
        	exit 1
fi

# Check if the OS matches CentOS
OS=`cat /etc/redhat-release | awk {'print $1}'`
if [ "$OS" != "CentOS" ]
        then
        echo -e "\e[1;31mSystem runs on something other than CentOS. This may not work!\e[0m";
        echo -e "\e[1;31mContinue Anyway? [y]\e[0m"
        read cont
        if [ $cont == "n" ] || [ $cont == "no"]; then
                echo "Exiting..."
                exit 1
        fi
fi

# Check if packages are already installed, if not install them
package=`rpm -qa | grep php-pear`
if [ "$package" == "" ]
	then
		yum install php-pear -y
fi
package=`rpm -qa | grep openssl-devel`
if [ "$package" == "" ]
	then
        	yum install openssl-devel -y
fi
package=`rpm -qa | grep libgcrypt-devel`
if [ "$package" == "" ]
	then
        	yum install libgcrypt-devel -y
fi
package=`rpm -qa | grep php53`
if [ "$package" == "" ]
	then
		yum install php-devel -y
	else
		yum install php53-devel -y
fi	

mkdir /tmp/libssh2
wd=`pwd`
cd /tmp/libssh2
wget http://www.libssh2.org/download/libssh2-1.3.0.tar.gz
tar -xvf libssh2-1.3.0.tar.gz && cd libssh2-1.3.0 && ./configure && make && make install
if [[ $? -ne 0 ]]
        then
                echo "LibSSH2 install failed. Please fix the errors and re-run this script."
		cd $wd
		rm -rf /tmp/libssh2
                exit 1
	else
		cd $wd
		rm -rf /tmp/libssh2
fi

pecl install channel://pecl.php.net/ssh2-0.11.3
if [[ $? -ne 0 ]]
        then
                echo "PECL install command failed. Please fix the errors and re-run this script."
                exit 1
fi

# Check to update php.ini
echo "Would you like to add \"extension=ssh2.so\" to the php.ini configuration file? [n]"
read add
if [ "$add" == "yes" ] || [ "$add" == "y" ]
	then
		echo "extension=ssh2.so" >> php.ini
		echo "SSH2 module successfully appended to php.ini configuration."
fi

# Check to restart apache
echo "Would you like to restart apache? [n]"
read apache
if [ "$apache" == "yes" ] || [ "$apache" == "y" ]
	then
		service httpd restart
fi
echo "All done!"
