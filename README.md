## README
This script is intended to install the correct packages and modules for CentOS so that Wordpress can autoupdate plugins, themes, etc using SSH.

## PREREQS
* CentOS
* A working Wordpress install
* root access

## INSTRUCTIONS
1. Run script as root
2. Follow the prompts.
3. That's it!
